﻿CREATE TABLE [dbo].[TblPadrinho] (
[Id_padrinho] INT IDENTITY (1, 1) NOT NULL,
[Nome] NVARCHAR (250) NULL,
[Data_nasc] DATETIME NULL,
[CPF] NVARCHAR (20) NULL,
[Email] NVARCHAR (45) NULL,
[Profissao] NVARCHAR (45) NULL,
PRIMARY KEY CLUSTERED ([Id_padrinho] ASC)
);

CREATE TABLE [dbo].[TblCrianca] (
[Id_crianca] INT IDENTITY (1, 1) NOT NULL,
[Nome] NVARCHAR (250) NULL,
[Data_nasc] DATETIME NULL,
[Presente] NVARCHAR (20) NULL,
[Carta] NVARCHAR (700) NULL,
[Instituicao] NVARCHAR (45) NULL,
[Status] INT NULL,
PRIMARY KEY CLUSTERED ([Id_crianca] ASC)
);
