﻿using Newtonsoft.Json;
using SocialNeppo.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace SocialNeppo.Controllers
{
    public class BaseAPIController : ApiController
    {
        protected readonly SocialNeppoDBEntities SocialNeppoDB = new SocialNeppoDBEntities(); // objeto classe a qual podemos chamar
                                                                                // metodos para caregar, adicionar, eatualizar e excluir usuarios..

        protected HttpResponseMessage ToJson(dynamic obj)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
                // toJson e um metodo tomado para qualquer tipo de objeto classe crianto o HTTP obejto resposta com ok de httpstatuscode e serializando o objeto
            return response;
        }
    }
}
