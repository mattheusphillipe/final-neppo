"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var home_component_1 = require("./components/home.component");
var user_component_1 = require("./components/user.component");
var crianca_component_1 = require("./components/crianca.component");
var login_component_1 = require("./components/login.component");
var loginCrianca_component_1 = require("./components/loginCrianca.component");
var appRoutes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: 'user', component: user_component_1.UserComponent },
    { path: 'crianca', component: crianca_component_1.CriancaComponent },
    // { path: 'home/loginCrianca', component: LoginCriancaComponent },
    { path: 'home/login', component: login_component_1.LoginComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'loginCrianca', component: loginCrianca_component_1.LoginCriancaComponent },
    { path: 'login/loginCrianca', component: loginCrianca_component_1.LoginCriancaComponent },
    { path: 'home/loginCrianca', component: loginCrianca_component_1.LoginCriancaComponent },
    { path: 'loginCrianca/login', component: login_component_1.LoginComponent }
    //  { path: 'teste', component: TesteComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map