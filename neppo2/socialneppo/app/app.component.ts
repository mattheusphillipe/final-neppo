﻿import { Component } from "@angular/core";
import { UserComponent } from './components/user.component';
import { CriancaComponent } from './components/crianca.component';


@Component
    (

    {
        
        selector: 'user-app', // se der erro era ""
        template: `                     
                      <nav class='navbar navbar-default label-primary '>
                           <div class='container-fluid'>
                             <ul class='nav navbar-nav'>                               
                               <li></li> 
                            &nbsp;
                            <a class="btn btn-primary navbar-btn" [routerLink]="['home']">O Projeto</a>
                            <a class="btn btn-primary navbar-btn" [routerLink]="['user']">Padrinho</a>
                            <a class="btn btn-primary navbar-btn"[routerLink]="['crianca']">Criança</a>
                            <a [routerLink]="['loginCrianca']" class="btn btn-success">Cadastrar/Login</a>
                           
                           
                            
                            </ul>        

                          </div>
                     </nav>    
                  <div class='container'>
                    <router-outlet></router-outlet>
                  </div>
                 

            

    `

        }
    )

export class AppComponent
{

}