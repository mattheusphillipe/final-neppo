﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { DBOperation } from '../Shared/enum';
import { Observable } from 'rxjs/Rx';
import { Global } from '../Shared/global';
import { UserComponent } from './user.component';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CriancaComponent } from './crianca.component';
import { LoginCriancaService } from '../Service/loginCrianca.service';


@Component({
       moduleId: module.id,
       templateUrl: './loginCrianca.component.html'
})



export class LoginCriancaComponent extends CriancaComponent {


}