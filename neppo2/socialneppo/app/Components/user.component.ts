﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../Service/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { IUser } from '../Models/user';
import { DBOperation } from '../Shared/enum';
import { Observable } from 'rxjs/Rx';
import { Global } from '../Shared/global';
import { FormsModule } from '@angular/forms';
import { CriancaComponent } from './crianca.component';

@Component({

    templateUrl: 'app/components/user.component.html'
})

export class UserComponent  implements OnInit {
    @ViewChild('modal') modal: ModalComponent;
    users: IUser[];
    user: IUser;
    msg: string;
    indLoading: boolean = false;
    userFrm: FormGroup;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;

    constructor(private fb: FormBuilder, private _userService: UserService) {
        
    }

    ngOnInit(): void {

        this.userFrm = this.fb.group({
            Id_padrinho: [''],
            Nome: ['', Validators.required],
            CPF: ['', Validators.required],
            Email: ['', Validators.required],
            Profissao: [''],
            Data_nasc: ['', Validators.required],
            Genero: ['', Validators.required],
            Telefone: ['', Validators.required]
        });

        this.LoadUsers();

    }

  

    LoadUsers(): void {
        this.indLoading = true;
        this._userService.get(Global.BASE_USER_ENDPOINT)
            .subscribe(users => { this.users = users; this.indLoading = false; },
            error => this.msg = <any>error);

    }

    addUser() {
        this.dbops = DBOperation.create;
        this.SetControlsState(true);
        this.modalTitle = "Adicionar novo padrinho";
        this.modalBtnTitle = "Adicionar";
        this.userFrm.reset();
        this.modal.open();
    }

    getUser(id: number) {
        this.SetControlsState(false);
        this.modalTitle = "Visualizar Padrinho";
        this.user = this.users.filter(x => x.Id_padrinho == id)[0];
        this.modal.open();
    }

    editUser(id: number) {
        this.dbops = DBOperation.update;
        this.SetControlsState(true);
        this.modalTitle = "Editar Padrinho";
        this.modalBtnTitle = "Atualizar";
        this.user = this.users.filter(x => x.Id_padrinho == id)[0];
        this.userFrm.setValue(this.user);
        this.modal.open();
    }

    deleteUser(id: number) {
        this.dbops = DBOperation.delete;
        this.SetControlsState(false);
        this.modalTitle = "Confirmar Remoção";
        this.modalBtnTitle = "Remover";
        this.user = this.users.filter(x => x.Id_padrinho == id)[0];
        this.userFrm.setValue(this.user);
        this.modal.open();
    }

    SetControlsState(isEnable: boolean) {
        isEnable ? this.userFrm.enable() : this.userFrm.disable();
    }

    onSubmit(formData: any) {
        this.msg = "";

        switch (this.dbops) {
            case DBOperation.create:
                this._userService.post(Global.BASE_USER_ENDPOINT, formData._value).subscribe(
                    data => {
                        if (data == 1) //Success
                        {
                            this.msg = "Adicionado com sucesso.";
                            this.LoadUsers();
                        }
                        else {
                            this.msg = "There is some issue in saving records, please contact to system administrator!";
                        }

                        this.modal.dismiss();
                    },
                    error => {
                        this.msg = error;
                    }
                );
                break;
            case DBOperation.update:
                this._userService.put(Global.BASE_USER_ENDPOINT, formData._value.Id_padrinho, formData._value).subscribe(
                    data => {
                        if (data == 1) //Success
                        {
                            this.msg = "Atualizado com sucesso";
                            this.LoadUsers();
                        }
                        else {

                            this.msg = "There is some issue in saving records, please contact to system administrator!";
                        }

                        this.modal.dismiss();
                    },
                    error => {
                        this.msg = error;
                    }
                );
                break;
            case DBOperation.delete:
                this._userService.delete(Global.BASE_USER_ENDPOINT, formData._value.Id_padrinho).subscribe(
                    data => {
                        if (data == 1) //Success
                        {
                            this.msg = "Removido com sucesso";
                            this.LoadUsers();
                        }
                        else {
                            this.msg = "There is some issue in saving records, please contact to system administrator!";
                        }

                        this.modal.dismiss();
                    },
                    error => {
                        this.msg = error;
                    }
                );
                break;

        }
    }

}