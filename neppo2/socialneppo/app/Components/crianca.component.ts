﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { CriancaService } from '../Service/crianca.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Icrianca } from '../Models/crianca';
import { DBOperation } from '../Shared/enum';
import { Observable } from 'rxjs/Rx';
import { Global } from '../Shared/global';
import { FormsModule } from '@angular/forms';

@Component({

    templateUrl: 'app/components/crianca.component.html'
})

export class CriancaComponent implements OnInit {
    @ViewChild('modal') modal: ModalComponent;
    criancas: Icrianca[];
    crianca: Icrianca;
    msg: string;
    indLoading: boolean = false;
    criancaFrm: FormGroup;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;

    constructor(private fb: FormBuilder, private _criancasService: CriancaService) { }

    ngOnInit(): void {

        this.criancaFrm = this.fb.group({

            Id_crianca: [''],
            Nome: ['', Validators.required],
            Data_nasc: ['', Validators.required],
            Presente: ['', Validators.required],
            Carta: [''],
            Instituicao: [''],
            Status: [''],
            Telefone: ['', Validators.required],
            Endereco: ['', Validators.required]

        });

        this.LoadCriancas();

    }



    LoadCriancas(): void {
        this.indLoading = true;
        this._criancasService.get(Global.BASE_USER_ENDPOINT2)
            .subscribe(criancas => { this.criancas = criancas; this.indLoading = false; },
                error => this.msg = <any>error);

    }

    addCrianca() {
        this.dbops = DBOperation.create;
        this.SetControlsState(true);
        this.modalTitle = "Adicionar nova Criança";
        this.modalBtnTitle = "Adicionar";
        this.criancaFrm.reset();
        this.modal.open();
    }

    getCrianca(id: number) {
        this.SetControlsState(false);
        this.modalTitle = "Visualizar Crianca";
        this.crianca = this.criancas.filter(x => x.Id_crianca == id)[0];
        this.criancaFrm.setValue(this.crianca);
        this.modal.open();
    }
    editCrianca(id: number) {
        this.dbops = DBOperation.update;
        this.SetControlsState(true);
        this.modalTitle = "Editar Criança";
        this.modalBtnTitle = "Atualizar";
        this.crianca = this.criancas.filter(x => x.Id_crianca == id)[0];
        this.criancaFrm.setValue(this.crianca);
        this.modal.open();
    }

    deleteCrianca(id: number) {
        this.dbops = DBOperation.delete;
        this.SetControlsState(false);
        this.modalTitle = "Confirmar Remoção?";
        this.modalBtnTitle = "Remover";
        this.crianca = this.criancas.filter(x => x.Id_crianca == id)[0];
        this.criancaFrm.setValue(this.crianca);
        this.modal.open();
    }

    SetControlsState(isEnable: boolean) {
        isEnable ? this.criancaFrm.enable() : this.criancaFrm.disable();
    }

    onSubmit(formData: any) {
        this.msg = "";

        switch (this.dbops) {
            case DBOperation.create: //1  var 1 bd create
                this._criancasService.post(Global.BASE_USER_ENDPOINT2, formData._value).subscribe(
                    data => {
                        if (data == 1) //Success
                        {
                            this.msg = "Adicionado com sucesso.";
                            this.LoadCriancas();
                        }
                        else {
                            this.msg = "There is some issue in saving records, please contact to system administrator!";
                        }

                        this.modal.dismiss();
                    },
                    error => {
                        this.msg = error;
                    }
                );
                break;
            case DBOperation.update: // enum var 2 bd update
                this._criancasService.put(Global.BASE_USER_ENDPOINT2, formData._value.Id_crianca, formData._value).subscribe(
                    data => {
                        if (data == 1) //Success
                        {
                            this.msg = "Atualizado com sucesso";
                            this.LoadCriancas();
                        }
                        else {
                            this.msg = "There is some issue in saving records, please contact to system administrator!";
                        }

                        this.modal.dismiss();
                    },
                    error => {
                        this.msg = error;
                    }
                );
                break;
            case DBOperation.delete: // enum var 2 bd delete
                this._criancasService.delete(Global.BASE_USER_ENDPOINT2, formData._value.Id_crianca).subscribe(
                    data => {
                        if (data == 1) //Success
                        {
                            this.msg = "Removido com sucesso";
                            this.LoadCriancas();
                        }
                        else {
                            this.msg = "There is some issue in saving records, please contact to system administrator!";
                        }

                        this.modal.dismiss();
                    },
                    error => {
                        this.msg = error;
                    }
                );
                break;

        }
    }

}