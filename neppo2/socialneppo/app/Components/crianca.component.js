"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var crianca_service_1 = require("../Service/crianca.service");
var forms_1 = require("@angular/forms");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var enum_1 = require("../Shared/enum");
var global_1 = require("../Shared/global");
var CriancaComponent = /** @class */ (function () {
    function CriancaComponent(fb, _criancasService) {
        this.fb = fb;
        this._criancasService = _criancasService;
        this.indLoading = false;
    }
    CriancaComponent.prototype.ngOnInit = function () {
        this.criancaFrm = this.fb.group({
            Id_crianca: [''],
            Nome: ['', forms_1.Validators.required],
            Data_nasc: ['', forms_1.Validators.required],
            Presente: ['', forms_1.Validators.required],
            Carta: [''],
            Instituicao: [''],
            Status: [''],
            Telefone: ['', forms_1.Validators.required],
            Endereco: ['', forms_1.Validators.required]
        });
        this.LoadCriancas();
    };
    CriancaComponent.prototype.LoadCriancas = function () {
        var _this = this;
        this.indLoading = true;
        this._criancasService.get(global_1.Global.BASE_USER_ENDPOINT2)
            .subscribe(function (criancas) { _this.criancas = criancas; _this.indLoading = false; }, function (error) { return _this.msg = error; });
    };
    CriancaComponent.prototype.addCrianca = function () {
        this.dbops = enum_1.DBOperation.create;
        this.SetControlsState(true);
        this.modalTitle = "Adicionar nova Criança";
        this.modalBtnTitle = "Adicionar";
        this.criancaFrm.reset();
        this.modal.open();
    };
    CriancaComponent.prototype.getCrianca = function (id) {
        this.SetControlsState(false);
        this.modalTitle = "Visualizar Crianca";
        this.crianca = this.criancas.filter(function (x) { return x.Id_crianca == id; })[0];
        this.criancaFrm.setValue(this.crianca);
        this.modal.open();
    };
    CriancaComponent.prototype.editCrianca = function (id) {
        this.dbops = enum_1.DBOperation.update;
        this.SetControlsState(true);
        this.modalTitle = "Editar Criança";
        this.modalBtnTitle = "Atualizar";
        this.crianca = this.criancas.filter(function (x) { return x.Id_crianca == id; })[0];
        this.criancaFrm.setValue(this.crianca);
        this.modal.open();
    };
    CriancaComponent.prototype.deleteCrianca = function (id) {
        this.dbops = enum_1.DBOperation.delete;
        this.SetControlsState(false);
        this.modalTitle = "Confirmar Remoção?";
        this.modalBtnTitle = "Remover";
        this.crianca = this.criancas.filter(function (x) { return x.Id_crianca == id; })[0];
        this.criancaFrm.setValue(this.crianca);
        this.modal.open();
    };
    CriancaComponent.prototype.SetControlsState = function (isEnable) {
        isEnable ? this.criancaFrm.enable() : this.criancaFrm.disable();
    };
    CriancaComponent.prototype.onSubmit = function (formData) {
        var _this = this;
        this.msg = "";
        switch (this.dbops) {
            case enum_1.DBOperation.create://1  var 1 bd create
                this._criancasService.post(global_1.Global.BASE_USER_ENDPOINT2, formData._value).subscribe(function (data) {
                    if (data == 1) {
                        _this.msg = "Adicionado com sucesso.";
                        _this.LoadCriancas();
                    }
                    else {
                        _this.msg = "There is some issue in saving records, please contact to system administrator!";
                    }
                    _this.modal.dismiss();
                }, function (error) {
                    _this.msg = error;
                });
                break;
            case enum_1.DBOperation.update:// enum var 2 bd update
                this._criancasService.put(global_1.Global.BASE_USER_ENDPOINT2, formData._value.Id_crianca, formData._value).subscribe(function (data) {
                    if (data == 1) {
                        _this.msg = "Atualizado com sucesso";
                        _this.LoadCriancas();
                    }
                    else {
                        _this.msg = "There is some issue in saving records, please contact to system administrator!";
                    }
                    _this.modal.dismiss();
                }, function (error) {
                    _this.msg = error;
                });
                break;
            case enum_1.DBOperation.delete:// enum var 2 bd delete
                this._criancasService.delete(global_1.Global.BASE_USER_ENDPOINT2, formData._value.Id_crianca).subscribe(function (data) {
                    if (data == 1) {
                        _this.msg = "Removido com sucesso";
                        _this.LoadCriancas();
                    }
                    else {
                        _this.msg = "There is some issue in saving records, please contact to system administrator!";
                    }
                    _this.modal.dismiss();
                }, function (error) {
                    _this.msg = error;
                });
                break;
        }
    };
    __decorate([
        core_1.ViewChild('modal'),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], CriancaComponent.prototype, "modal", void 0);
    CriancaComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/components/crianca.component.html'
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder, crianca_service_1.CriancaService])
    ], CriancaComponent);
    return CriancaComponent;
}());
exports.CriancaComponent = CriancaComponent;
//# sourceMappingURL=crianca.component.js.map