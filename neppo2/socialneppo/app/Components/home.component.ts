﻿import { Component } from "@angular/core";

@Component({
   
    template: ` <div class='panel panel-primary'>
    
    <div class='panel-body'>


<h3>O projeto</h3>

<p>
O Projeto Natal Solidário da Neppo consiste em cadastrar cartinhas de algumas crianças de instituições da cidade de Uberlândia para que os funcionários possam de 
maneira solidária
ser um Padrinho Noel e contribuir com um pedido realizado pela criança através da cartinha escolhida.
</p>
<div class="row" style="text-align: center">

<div class="col-sm-6">

<img src="/images/image5.jpg" style="width:100%; height:auto;" class="img-rounded" alt="Responsive image">

</div>

</div>
<h3>
Como adotar?
</h3>

<p>
Para adotar é muito simples, basta cadastrar seus dados como Padrinho Noel, escolher a cartinha que será adotada e, após isso, levar o 
presente até a equipe administrativa da Neppo, pois eles cuidarão para levar o tão sonhado presente até a criança! 
</p>

<h4>É muito fácil, não é?! O que está esperando então ?</h4>

<h5>Clique aqui para adotar uma cartinha e, neste Natal, seja você também um Padrinho Noel! </h5>
<a [routerLink]="['loginCrianca']" class="btn btn-success">Cadastrar/Login</a>
</div>
</div>

`
})

export class HomeComponent { }