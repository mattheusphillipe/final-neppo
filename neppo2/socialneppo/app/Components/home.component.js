"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent = __decorate([
        core_1.Component({
            template: " <div class='panel panel-primary'>\n    \n    <div class='panel-body'>\n\n\n<h3>O projeto</h3>\n\n<p>\nO Projeto Natal Solid\u00E1rio da Neppo consiste em cadastrar cartinhas de algumas crian\u00E7as de institui\u00E7\u00F5es da cidade de Uberl\u00E2ndia para que os funcion\u00E1rios possam de \nmaneira solid\u00E1ria\nser um Padrinho Noel e contribuir com um pedido realizado pela crian\u00E7a atrav\u00E9s da cartinha escolhida.\n</p>\n<div class=\"row\" style=\"text-align: center\">\n\n<div class=\"col-sm-6\">\n\n<img src=\"/images/image5.jpg\" style=\"width:100%; height:auto;\" class=\"img-rounded\" alt=\"Responsive image\">\n\n</div>\n\n</div>\n<h3>\nComo adotar?\n</h3>\n\n<p>\nPara adotar \u00E9 muito simples, basta cadastrar seus dados como Padrinho Noel, escolher a cartinha que ser\u00E1 adotada e, ap\u00F3s isso, levar o \npresente at\u00E9 a equipe administrativa da Neppo, pois eles cuidar\u00E3o para levar o t\u00E3o sonhado presente at\u00E9 a crian\u00E7a! \n</p>\n\n<h4>\u00C9 muito f\u00E1cil, n\u00E3o \u00E9?! O que est\u00E1 esperando ent\u00E3o ?</h4>\n\n<h5>Clique aqui para adotar uma cartinha e, neste Natal, seja voc\u00EA tamb\u00E9m um Padrinho Noel! </h5>\n<a [routerLink]=\"['loginCrianca']\" class=\"btn btn-success\">Cadastrar/Login</a>\n</div>\n</div>\n\n"
        })
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map