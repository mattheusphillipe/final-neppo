﻿import { NgModule } from '@angular/core'; //, NgModule do núcleo angular
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { HomeComponent } from './components/home.component';
//import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './components/user.component';
import { CriancaComponent } from './components/crianca.component';
import {LoginComponent } from './components/login.component';
import { LoginCriancaComponent } from './components/loginCrianca.component';

import { UserService } from './Service/user.service';
import { LoginService } from './Service/login.service';
import { LoginCriancaService } from './Service/loginCrianca.service';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { CriancaService } from './Service/crianca.service'; 



@NgModule({
    imports: [BrowserModule, ReactiveFormsModule, HttpModule, routing, Ng2Bs3ModalModule], //contém lista de módulos.
    declarations: [AppComponent, HomeComponent, UserComponent, CriancaComponent, LoginCriancaComponent ,LoginComponent], //contém lista de componentes,
    providers: [{ provide: APP_BASE_HREF, useValue: '/' }, UserService, CriancaService], //contém a lista de serviços. Vamos adicionar serviço com HTTP operações para 
                                                                        //executar operações de leitura, adição, atualização e exclusão de usuários.
    bootstrap: [AppComponent] //  entrada aos componentes
})

export class AppModule { }