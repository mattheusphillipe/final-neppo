﻿export interface IUser {
    Id_padrinho: number,
    Nome: string,
    Email: string,
    CPF: string,
    Profissao: string,
    Data_nasc: string,
    Genero: string,
    Telefone:string
}