﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home.component';
import { UserComponent } from './components/user.component';
import { CriancaComponent } from './components/crianca.component';
import { LoginComponent } from './components/login.component';
import { LoginCriancaComponent } from './components/loginCrianca.component';
import { TesteComponent } from './components/teste.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'user', component: UserComponent},
    { path: 'crianca', component: CriancaComponent },   
   // { path: 'home/loginCrianca', component: LoginCriancaComponent },
    { path: 'home/login', component: LoginComponent },
    { path: 'login', component: LoginComponent },
    { path: 'loginCrianca', component: LoginCriancaComponent },
    { path: 'login/loginCrianca', component: LoginCriancaComponent }, 
    { path: 'home/loginCrianca', component: LoginCriancaComponent }, 
    { path: 'loginCrianca/login', component: LoginComponent }
  //  { path: 'teste', component: TesteComponent }
   
    
    
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);